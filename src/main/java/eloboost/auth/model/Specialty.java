package eloboost.auth.model;

public enum Specialty {
    VALORANT, DOTA2, LEAGUEOFLEGENDS
}
