package eloboost.auth.model;

import jakarta.persistence.*;
import lombok.*;

@Setter
@Getter
@ToString
@RequiredArgsConstructor
@Entity
@DiscriminatorValue("WORKER")
public class Worker extends User {
    private String discordId;

    @Enumerated(EnumType.STRING)
    private Specialty specialty;
}
