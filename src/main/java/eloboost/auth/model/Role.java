package eloboost.auth.model;

public enum Role {
    USER, WORKER, ADMIN
}