package eloboost.auth.model;

import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;
import lombok.*;

@Setter
@Getter
@ToString
@RequiredArgsConstructor
@Entity
@DiscriminatorValue("ADMIN")
public class Admin extends User {
    //
}
