package eloboost.auth.controller;

import eloboost.auth.dto.AuthResponse;
import eloboost.auth.dto.LoginRequest;
import eloboost.auth.dto.RegisterRequest;
import eloboost.auth.dto.UserResponse;
import eloboost.auth.model.*;
import eloboost.auth.service.UserService;
import eloboost.auth.service.JwtUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/auth")
public class AuthController {

    private final UserService userService;
    private final JwtUtils jwtUtils;

    public AuthController(UserService userService, JwtUtils jwtUtils) {
        this.userService = userService;
        this.jwtUtils = jwtUtils;
    }

    @PostMapping("/register")
    public ResponseEntity<UserResponse> registerUser(@RequestBody RegisterRequest registerRequest) {
        User user;
        Role role = Role.valueOf(registerRequest.getRole().toUpperCase());
        switch (role) {
            case WORKER:
                Worker worker = new Worker();
                worker.setEmail(registerRequest.getEmail());
                worker.setPassword(registerRequest.getPassword());
                worker.setName(registerRequest.getName());
                worker.setPhoneNumber(registerRequest.getPhoneNumber());
                worker.setDiscordId(registerRequest.getDiscordId());
                worker.setSpecialty(Specialty.valueOf(registerRequest.getSpecialty()));
                worker.setRole(Role.WORKER);
                user = worker;
                break;
            case ADMIN:
                Admin admin = new Admin();
                admin.setEmail(registerRequest.getEmail());
                admin.setPassword(registerRequest.getPassword());
                admin.setName(registerRequest.getName());
                admin.setPhoneNumber(registerRequest.getPhoneNumber());
                admin.setRole(Role.ADMIN);
                user = admin;
                break;
            default:
                user = new User();
                user.setEmail(registerRequest.getEmail());
                user.setPassword(registerRequest.getPassword());
                user.setName(registerRequest.getName());
                user.setPhoneNumber(registerRequest.getPhoneNumber());
                user.setRole(Role.USER);
        }

        User savedUser = userService.saveUser(user);
        UserResponse userResponse = convertToUserResponse(savedUser);

        return ResponseEntity.ok(userResponse);
    }

    @PostMapping("/login")
    public ResponseEntity<AuthResponse> loginUser(@RequestBody LoginRequest loginRequest) {
        User user = userService.authenticate(loginRequest.getEmail(), loginRequest.getPassword());
        String token = jwtUtils.generateToken(user.getEmail());
        UserResponse userResponse = convertToUserResponse(user);

        AuthResponse authResponse = new AuthResponse();
        authResponse.setToken(token);
        authResponse.setUser(userResponse);

        return ResponseEntity.ok(authResponse);
    }

    private UserResponse convertToUserResponse(User user) {
        UserResponse userResponse = new UserResponse();
        userResponse.setId(String.valueOf(user.getId()));
        userResponse.setEmail(user.getEmail());
        userResponse.setName(user.getName());
        userResponse.setPhoneNumber(user.getPhoneNumber());
        userResponse.setRole(user.getRole().name());

        if (user instanceof Worker) {
            userResponse.setDiscordId(((Worker) user).getDiscordId());
            userResponse.setSpecialty(((Worker) user).getSpecialty().toString());
        }

        return userResponse;
    }
}
