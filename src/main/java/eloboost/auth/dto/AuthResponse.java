package eloboost.auth.dto;

import lombok.Data;

@Data
public class AuthResponse {
    private String token;
    private UserResponse user;
}
