package eloboost.auth.dto;

import lombok.Data;

@Data
public class UserResponse {
    private String id;
    private String email;
    private String name;
    private String phoneNumber;
    private String discordId; // For worker only
    private String specialty; // For worker only
    private String role;
}
