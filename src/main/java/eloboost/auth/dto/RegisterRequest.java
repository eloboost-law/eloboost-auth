package eloboost.auth.dto;

import lombok.Data;

@Data
public class RegisterRequest {
    private String email;
    private String password;
    private String name;
    private String phoneNumber;
    private String discordId; // worker only
    private String specialty; // worker only
    private String role; // worker only
}
